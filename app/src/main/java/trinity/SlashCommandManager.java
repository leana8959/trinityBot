package trinity;

import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.events.interaction.command.SlashCommandInteractionEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;
import org.jetbrains.annotations.NotNull;
import trinity.slash_command.*;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class SlashCommandManager extends ListenerAdapter {

	private static SlashCommandManager instance;
	List<SlashCommand> commands = new ArrayList<>();

	public SlashCommandManager() {
		addCommand(new HeyCommand());
		addCommand(new PingCommand());

		addCommand(new JoinCommand());
		addCommand(new LeaveCommand());

		addCommand(new PlayCommand());
		addCommand(new PauseCommand());
		addCommand(new ResumeCommand());
		addCommand(new StopCommand());

		addCommand(new NextCommand());
		addCommand(new PrevCommand());

		addCommand(new ShuffleCommand());
		addCommand(new ClearCommand());
		addCommand(new InfoCommand());
//		addCommand(new RepeatCommand()); Deprecated

	}

	/**
	 * Register the list of commands so that users can see them in their clients
	 *
	 * @param jda The JDA object
	 */
	public void updateCommands(JDA jda) throws InterruptedException {
		jda.awaitReady()
				.updateCommands()
				.addCommands(
						this.commands.stream().map(x -> x.getSlashCommand()).collect(Collectors.toList())
				).queue();
	}

	/**
	 * Parse the event and hand it off to each commands
	 *
	 * @param event
	 */
	@Override
	public void onSlashCommandInteraction(@NotNull SlashCommandInteractionEvent event) {
		this.commands.stream().filter(x -> x.name().equals(event.getName())).findFirst().get().handle(event);
	}

	/**
	 * Invoke another command to complete the task
	 *
	 * @param event
	 * @param commandName
	 */
	public void invoke(String commandName, SlashCommandInteractionEvent event) {
		this.commands.stream()
				.filter(x -> x.name().equals(commandName))
				.findFirst()
				.get()
				.handle(event);
	}

	public void invokeDefer(String commandName, SlashCommandInteractionEvent event) {
		this.commands.stream()
				.filter(x -> x instanceof MusicSlashCommand)
				.map(x -> (MusicSlashCommand) x)
				.filter(x -> x.name().equals(commandName))
				.findFirst()
				.get()
				.handleDefer(event);
	}

	private void addCommand(SlashCommand cmd) {
		assert cmd.name() != null;
		assert cmd.help() != null;
		assert cmd.getSlashCommand() != null;
		assert this.commands.stream().filter(x -> x.name().equals(cmd.name())).collect(Collectors.toList()).isEmpty();

		commands.add(cmd);
	}

	public static SlashCommandManager getInstance() {
		if (instance == null) {
			instance = new SlashCommandManager();
		}
		return instance;
	}
}
