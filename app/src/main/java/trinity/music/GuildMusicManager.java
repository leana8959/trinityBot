package trinity.music;

import com.sedmelluq.discord.lavaplayer.player.AudioLoadResultHandler;
import com.sedmelluq.discord.lavaplayer.player.AudioPlayerManager;
import com.sedmelluq.discord.lavaplayer.player.DefaultAudioPlayerManager;
import com.sedmelluq.discord.lavaplayer.source.AudioSourceManagers;
import com.sedmelluq.discord.lavaplayer.tools.FriendlyException;
import com.sedmelluq.discord.lavaplayer.track.AudioPlaylist;
import com.sedmelluq.discord.lavaplayer.track.AudioTrack;
import com.sedmelluq.discord.lavaplayer.track.AudioTrackInfo;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.entities.Guild;
import net.dv8tion.jda.api.events.interaction.command.SlashCommandInteractionEvent;
import trinity.slash_command.MusicSlashCommand;
import trinity.slash_command.PlayCommand;

import java.awt.*;
import java.util.HashMap;
import java.util.Map;

public class GuildMusicManager {
	private static GuildMusicManager instance;

	private final Map<Long, MusicManager> guildId2MusicManager;
	private final AudioPlayerManager audioPlayerManager;

	public GuildMusicManager() {
		this.guildId2MusicManager = new HashMap<>();
		this.audioPlayerManager = new DefaultAudioPlayerManager();

		AudioSourceManagers.registerRemoteSources(this.audioPlayerManager);
	}

	/**
	 * Return an instance of GuildMusicManager per guild, based on guild ID.
	 *
	 * @param guild
	 * @return A unique GuildMusicManager for the guild.
	 */
	public MusicManager getMusicManager(Guild guild) {
		return this.guildId2MusicManager.computeIfAbsent(guild.getIdLong(), (guildId) -> {
			final MusicManager musicManager = new MusicManager(this);
			guild.getAudioManager().setSendingHandler(musicManager.getSendHandler());
			return musicManager;
		});
	}

	public AudioPlayerManager getAudioPlayerManager() {
		return audioPlayerManager;
	}

	public static GuildMusicManager getInstance() {
		if (instance == null) {
			instance = new GuildMusicManager();
		}
		return instance;
	}
}
