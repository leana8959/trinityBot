package trinity.music;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class ArrayDeque<AudioTrack> extends ArrayList<AudioTrack> {

	public ArrayDeque() {
	}

	public ArrayDeque(@NotNull Collection<? extends AudioTrack> c) {
		super(c);
	}

	public AudioTrack popHead() throws IllegalStateException {
		if (this.size() <= 0) {
			throw new IndexOutOfBoundsException();
		}
		return this.remove(0);
	}

	public List<AudioTrack> popHead(int count) throws IndexOutOfBoundsException {
		if (this.size() <= 0) {
			throw new IndexOutOfBoundsException();
		}
		List<AudioTrack> popped = new ArrayList<>();
		popped.addAll(this.subList(0, count));
		this.subList(0, count).clear();
		return popped;
	}

	public AudioTrack popTail() throws IndexOutOfBoundsException {
		if (this.size() <= 0) {
			throw new IndexOutOfBoundsException();
		}
		return this.remove(this.size() - 1);
	}

	public void pushHead(AudioTrack track) {
		this.add(0, track);
	}

	public void pushHead(List<AudioTrack> tracks) {
		this.addAll(0, tracks);
	}

	public void pushTail(AudioTrack track) {
		this.add(track);
	}

	public void pushTail(List<AudioTrack> tracks) {
		this.addAll(tracks);
	}

}
