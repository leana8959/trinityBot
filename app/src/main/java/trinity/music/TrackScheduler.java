package trinity.music;

import com.sedmelluq.discord.lavaplayer.player.AudioLoadResultHandler;
import com.sedmelluq.discord.lavaplayer.player.AudioPlayer;
import com.sedmelluq.discord.lavaplayer.player.event.AudioEventAdapter;
import com.sedmelluq.discord.lavaplayer.tools.FriendlyException;
import com.sedmelluq.discord.lavaplayer.track.AudioPlaylist;
import com.sedmelluq.discord.lavaplayer.track.AudioTrack;
import com.sedmelluq.discord.lavaplayer.track.AudioTrackEndReason;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.events.interaction.command.SlashCommandInteractionEvent;
import trinity.SlashCommandManager;
import trinity.slash_command.*;

import java.awt.*;
import java.util.List;
import java.util.stream.Collectors;

public class TrackScheduler extends AudioEventAdapter {

	private MusicManager musicManager;
	private final AudioPlayer player;
	private final TrackQueue q;
	private SlashCommandInteractionEvent lastEvent;


	public TrackScheduler(MusicManager mgr) {
		this.musicManager = mgr;
		this.player = mgr.getPlayer();
		this.q = new TrackQueue();
	}

	public void play(PlayCommand command) {
		updateLastEvent(command.getEvent());
		final GuildMusicManager guildMusicManager = GuildMusicManager.getInstance();
		final MusicManager musicManager = guildMusicManager.getMusicManager(lastEvent.getGuild());

		this.musicManager.getAudioPlayerManager().loadItemOrdered(musicManager, command.getQuery(), new AudioLoadResultHandler() {
			@Override
			public void trackLoaded(AudioTrack track) {
				String title;
				if (command.isInterrupt()) {
					player.playTrack(track);
					q.setCurrentTrack(track);
					title = "Now playing";
				} else {
					if (command.isInsert()) {
						q.insert(track);
						title = "Coming up";
					} else {
						q.append(track);
						title = "Queued";
					}
				}

				EmbedBuilder builder = new EmbedBuilder()
						.setTitle(title)
						.addField(track.getInfo().title, track.getInfo().author, false)
						.setFooter(lastEvent.getMember().getNickname(),
								lastEvent.getMember().getAvatarUrl())
						.setColor(Color.GREEN);
				lastEvent.getHook().editOriginalEmbeds(builder.build()).queue();
			}

			@Override
			public void playlistLoaded(AudioPlaylist playlist) {
				List<AudioTrack> tracks = playlist.getTracks();
				final int size = playlist.getTracks().size();
				String title;
				if (command.isInterrupt()) {
					player.playTrack(tracks.get(0));
					q.append(tracks.subList(1, tracks.size()));
					q.setCurrentTrack(tracks.get(0));
					title = "Now playing " + size + " songs";
				} else {
					if (command.isInsert()) {
						q.insert(tracks);
						title = size + " songs coming up";
					} else {
						q.append(tracks);
						title = "Queued " + size + " songs";
					}
				}

				EmbedBuilder builder = new EmbedBuilder()
						.setTitle(title)
						.setDescription(playlist.getName())
						.setFooter(lastEvent.getMember().getNickname(),
								lastEvent.getMember().getAvatarUrl())
						.setColor(Color.GREEN);
				lastEvent.getHook().editOriginalEmbeds(builder.build()).queue();
			}

			@Override
			public void noMatches() {
				EmbedBuilder builder = new EmbedBuilder()
						.setTitle("I'm sorry I can't find your song...")
						.setColor(Color.RED);
				lastEvent.getHook().editOriginalEmbeds(builder.build())
						.queue();
			}

			@Override
			public void loadFailed(FriendlyException exception) {
				EmbedBuilder builder = new EmbedBuilder()
						.setTitle(exception.toString())
						.setColor(Color.RED);
				lastEvent.getHook().editOriginalEmbeds(builder.build())
						.queue();
			}
		});
	}

	public void pause(MusicSlashCommand command) {
		updateLastEvent(command.getEvent());
		if (!player.isPaused()) {
			player.setPaused(true);
			EmbedBuilder builder = new EmbedBuilder()
					.setTitle("Paused, go get some coffee")
					.setFooter(lastEvent.getMember().getNickname(),
							lastEvent.getMember().getAvatarUrl())
					.setColor(Color.GREEN);
			lastEvent.getHook().editOriginalEmbeds(builder.build()).queue();
		} else {
			EmbedBuilder builder = new EmbedBuilder()
					.setTitle("It's already paused")
					.setColor(Color.RED);
			lastEvent.getHook().editOriginalEmbeds(builder.build()).queue();
		}
	}

	public void resume(ResumeCommand command) {
		updateLastEvent(command.getEvent());
		if (player.isPaused()) {
			player.setPaused(false);
			EmbedBuilder builder = new EmbedBuilder()
					.setTitle("Music is back on !")
					.setFooter(lastEvent.getMember().getNickname(),
							lastEvent.getMember().getAvatarUrl())
					.setColor(Color.GREEN);
			lastEvent.getHook().editOriginalEmbeds(builder.build()).queue();
		} else {
			EmbedBuilder builder = new EmbedBuilder()
					.setTitle("Music's already on")
					.setColor(Color.RED);
			lastEvent.getHook().editOriginalEmbeds(builder.build()).queue();
		}
	}

	@Override
	public void onTrackEnd(AudioPlayer player, AudioTrack track, AudioTrackEndReason endReason) {
		if (endReason.mayStartNext) {
			if (q.hasNext()) {
				this.player.playTrack(this.q.getNext(1));
				if (this.lastEvent != null) {
					SlashCommandManager.getInstance()
							.invokeDefer("info", lastEvent);
				}
			} else {
				this.player.stopTrack();
				if (this.lastEvent != null) {
					EmbedBuilder builder = new EmbedBuilder()
							.setTitle("Music stops, the party's over")
							.setColor(Color.RED);
					lastEvent.getHook().editOriginalEmbeds(builder.build()).queue();
				}
			}
		}
	}

	public void stop(MusicSlashCommand command) {
		updateLastEvent(command.getEvent());
		if (player.isPaused()) {
			EmbedBuilder builder = new EmbedBuilder()
					.setTitle("There's nothing to pause")
					.setFooter(lastEvent.getMember().getNickname(),
							lastEvent.getMember().getAvatarUrl())
					.setColor(Color.RED);
			lastEvent.getHook().editOriginalEmbeds(builder.build()).queue();
		} else {
			player.stopTrack();
			q.clear();
			EmbedBuilder builder = new EmbedBuilder()
					.setTitle("Stopped, the party's over")
					.setFooter(lastEvent.getMember().getNickname(),
							lastEvent.getMember().getAvatarUrl())
					.setColor(Color.GREEN);
			lastEvent.getHook().editOriginalEmbeds(builder.build()).queue();
		}
	}

	public void next(NextCommand command) {
		int count = command.getCount();
		updateLastEvent(command.getEvent());
		if (q.hasNext()) {
			final AudioTrack track = this.q.getNext(count);
			this.player.playTrack(track);
			EmbedBuilder builder = new EmbedBuilder()
					.setTitle(count == 1 ? "Skipping" : "Skipping " + count + " songs")
					.addField(track.getInfo().title, track.getInfo().author, false)
					.setFooter(lastEvent.getMember().getNickname(),
							lastEvent.getMember().getAvatarUrl())
					.setColor(Color.GREEN);
			lastEvent.getHook().editOriginalEmbeds(builder.build()).queue();
		} else {
			EmbedBuilder builder = new EmbedBuilder()
					.setTitle("There's no next song")
					.setColor(Color.RED);
			lastEvent.getHook().editOriginalEmbeds(builder.build()).queue();
		}
	}

	public void prev(MusicSlashCommand command) {
		updateLastEvent(command.getEvent());
		if (q.hasPrev()) {
			final AudioTrack track = this.q.getPrev();
			this.player.playTrack(track);
			EmbedBuilder builder = new EmbedBuilder()
					.setTitle("Rewinding")
					.addField(track.getInfo().title, track.getInfo().author, false)
					.setFooter(lastEvent.getMember().getNickname(),
							lastEvent.getMember().getAvatarUrl())
					.setColor(Color.GREEN);
			lastEvent.getHook().editOriginalEmbeds(builder.build()).queue();
		} else {
			EmbedBuilder builder = new EmbedBuilder()
					.setTitle("There's no previous song")
					.setColor(Color.RED);
			lastEvent.getHook().editOriginalEmbeds(builder.build()).queue();
		}
	}

	public void shuffle(MusicSlashCommand command) {
		updateLastEvent(command.getEvent());
		this.q.shuffle();
		if (q.hasNext()) {
			EmbedBuilder builder = new EmbedBuilder()
					.setTitle("One *shuffling*, coming right up")
					.setFooter(lastEvent.getMember().getNickname(),
							lastEvent.getMember().getAvatarUrl())
					.setColor(Color.GREEN);
			lastEvent.getHook().editOriginalEmbeds(builder.build()).queue();
		} else {
			EmbedBuilder builder = new EmbedBuilder()
					.setTitle("There's nothing in the queue")
					.setColor(Color.RED);
			lastEvent.getHook().editOriginalEmbeds(builder.build()).queue();
		}
	}

	public void clear(MusicSlashCommand command) {
		updateLastEvent(command.getEvent());
		this.q.clear();
		EmbedBuilder builder = new EmbedBuilder()
				.setTitle("Cleared the queue!")
				.setFooter(lastEvent.getMember().getNickname(),
						lastEvent.getMember().getAvatarUrl())
				.setColor(Color.GREEN);
		lastEvent.getHook().editOriginalEmbeds(builder.build()).queue();
	}

	public void repeat(RepeatCommand command) {
		updateLastEvent(command.getEvent());
		final boolean on = command.isRepeat();
		if (this.q.isRepeat() != on) {
			this.q.setRepeat(on);
			EmbedBuilder builder = new EmbedBuilder()
					.setTitle("Turning " + (on ? "on" : "off") + " the repeat mode")
					.setFooter(lastEvent.getMember().getNickname(),
							lastEvent.getMember().getAvatarUrl())
					.setColor(Color.GREEN);
			lastEvent.getHook().editOriginalEmbeds(builder.build()).queue();
		} else {
			EmbedBuilder builder = new EmbedBuilder()
					.setTitle("Repeat mode is already " + (on ? "on" : "off"))
					.setColor(Color.RED);
			lastEvent.getHook().editOriginalEmbeds(builder.build()).queue();
		}
	}

	public void info(MusicSlashCommand command) {
		updateLastEvent(command.getEvent());
		EmbedBuilder builder = new EmbedBuilder();
		builder.setTitle("Currently playing🎹🎵")
				.setDescription(q.getCurrentTrack().getInfo().title
						+ " by " + q.getCurrentTrack().getInfo().author);

		if (q.getPrevs().size() > 0) {
			String text = q.getPrevs().subList(Math.max(q.getPrevs().size() - 5, 0), q.getPrevs().size())
					.stream()
					.map(track -> track.getInfo().title + " by " + track.getInfo().author)
					.collect(Collectors.joining("\n"));
			builder.addField("Last " + Math.min(q.getPrevs().size(), 5) + " songs: ", text, false);
		} else {
			builder.addField("There's nothing in the history", "", false);
		}

		final boolean repeat = q.isRepeat();
		if (q.getNexts().size() > 0) {
			String text = q.getNexts().subList(0, Math.min(q.getNexts().size(), 5))
					.stream()
					.map(track -> track.getInfo().title + " by " + track.getInfo().author)
					.collect(Collectors.joining("\n"));
			builder.addField("Next " + Math.min(q.getNexts().size(), 5) + " songs: ", text, false);
		} else {
			builder.addField(repeat ? "Looping is activated, will loop at the next song" : "There 's nothing more in the queue",
					"", false);
		}
		lastEvent.getHook().editOriginalEmbeds(builder.build()).queue();
	}

	private void updateLastEvent(SlashCommandInteractionEvent lastEvent) {
		if (lastEvent != null) {
			this.lastEvent = lastEvent;
		}
	}

}
