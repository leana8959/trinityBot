package trinity.music;


import com.sedmelluq.discord.lavaplayer.track.AudioTrack;

import java.util.Collections;
import java.util.List;

/**
 * Implements two queues, one contains songs that have been played and the other one those that haven't
 */
public class TrackQueue {

	private ArrayDeque<AudioTrack> nexts;
	private ArrayDeque<AudioTrack> prevs;
	private AudioTrack currentTrack;

	private boolean repeat;

	TrackQueue() {
		this.nexts = new ArrayDeque<>();
		this.repeat = false;
	}

	public ArrayDeque<AudioTrack> getPrevs() {
		return new ArrayDeque<>(this.prevs);
	}

	public ArrayDeque<AudioTrack> getNexts() {
		return new ArrayDeque<>(this.nexts);
	}

	public boolean isRepeat() {
		return repeat;
	}

	public void setRepeat(boolean repeat) {
		this.repeat = repeat;
	}

	public AudioTrack getCurrentTrack() {
		return currentTrack;
	}

	public void setCurrentTrack(AudioTrack currentTrack) {
		this.currentTrack = currentTrack;
	}

	void append(AudioTrack track) {
		this.nexts.pushTail(track);
	}

	void append(List<AudioTrack> list) {
		this.nexts.pushTail(list);
	}

	void insert(AudioTrack track) {
		this.nexts.pushHead(track);
	}

	void insert(List<AudioTrack> tracks) {
		this.nexts.pushHead(tracks);
	}

	boolean hasPrev() {
		return !this.prevs.isEmpty() || repeat;
	}

	boolean hasNext() {
		return !this.nexts.isEmpty() || repeat;
	}

	AudioTrack getPrev() throws IndexOutOfBoundsException {
		this.nexts.pushHead(this.currentTrack);
		if (prevs.isEmpty()) {
			loop();
		}

		this.currentTrack = this.prevs.popTail().makeClone();
		return this.currentTrack;
	}

	AudioTrack getNext() throws IndexOutOfBoundsException {
		this.prevs.pushTail(this.currentTrack);
		if (nexts.isEmpty()) {
			loop();
		}

		this.currentTrack = this.nexts.popHead();
		return this.currentTrack;
	}

	AudioTrack getNext(int count) throws IndexOutOfBoundsException {
		this.prevs.pushTail(this.currentTrack);
		if (nexts.isEmpty()) {
			loop();
		}

		final List<AudioTrack> audioTracks = this.nexts.popHead(count - 1);
		this.prevs.pushTail(audioTracks);
		this.currentTrack = this.nexts.popHead().makeClone();
		return this.currentTrack;
	}

	void loop() {
		ArrayDeque<AudioTrack> temp = this.nexts;
		this.nexts = this.prevs;
		this.prevs = temp;

		Collections.shuffle(nexts);
	}

	void clear() {
		this.prevs.clear();
		this.nexts.clear();
	}

	void shuffle() {
		Collections.shuffle(this.nexts);
	}
}
