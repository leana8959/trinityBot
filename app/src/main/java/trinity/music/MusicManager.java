package trinity.music;

import com.sedmelluq.discord.lavaplayer.player.AudioPlayer;
import com.sedmelluq.discord.lavaplayer.player.AudioPlayerManager;

public class MusicManager {

	private AudioPlayerManager audioPlayerManager;
	private final AudioPlayer player;
	private final TrackScheduler scheduler;
	private final AudioPlayerSendHandler sendHandler;

	public AudioPlayerSendHandler getSendHandler() {
		return sendHandler;
	}


	public MusicManager(GuildMusicManager mgr) {
		this.audioPlayerManager = mgr.getAudioPlayerManager();

		this.player = mgr.getAudioPlayerManager().createPlayer();

		this.scheduler = new TrackScheduler(this);
		this.player.addListener(this.scheduler);

		this.sendHandler = new AudioPlayerSendHandler(this.player);
	}

	public AudioPlayerManager getAudioPlayerManager() {
		return audioPlayerManager;
	}

	public AudioPlayer getPlayer() {
		return player;
	}

	public TrackScheduler getScheduler() {
		return scheduler;
	}
}
