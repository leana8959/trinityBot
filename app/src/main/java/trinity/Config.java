package trinity;

import io.github.cdimascio.dotenv.Dotenv;

/**
 * Read configuration from .env
 */
public class Config {

	private static final Dotenv dotenv = Dotenv.load();

	public static String get(String key) {
		return dotenv.get(key.toUpperCase());
	}
}
