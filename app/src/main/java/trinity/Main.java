package trinity;


import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.JDABuilder;
import net.dv8tion.jda.api.OnlineStatus;
import net.dv8tion.jda.api.entities.Activity;
import net.dv8tion.jda.api.hooks.ListenerAdapter;
import net.dv8tion.jda.api.utils.cache.CacheFlag;

import javax.security.auth.login.LoginException;

public class Main {

	public static void main(String[] args) throws InterruptedException {

		final SlashCommandManager slashCommandManager = SlashCommandManager.getInstance();
		JDA jda = JDABuilder.createDefault(Config.get("TOKEN"))
				.setActivity(Activity.watching("matrix glitching"))
				.setStatus(OnlineStatus.ONLINE)
				.enableCache(CacheFlag.VOICE_STATE)
				.addEventListeners(slashCommandManager)
				.build();
		slashCommandManager.updateCommands(jda);
	}


}