package trinity.slash_command;

import com.sedmelluq.discord.lavaplayer.track.AudioTrack;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.events.interaction.command.SlashCommandInteractionEvent;
import trinity.music.GuildMusicManager;
import trinity.music.TrackScheduler;

import java.awt.Color;
import java.util.List;
import java.util.stream.Collectors;

public class InfoCommand extends MusicSlashCommand {
	@Override
	public void handleDefer(SlashCommandInteractionEvent event) {

		if (!selfInVoiceChannel(event)) {
			EmbedBuilder builder = new EmbedBuilder()
					.setTitle("I have to be in a audio channel for this to work")
					.setColor(Color.RED);
			event.getHook().editOriginalEmbeds(builder.build()).queue();
			return;
		}

		GuildMusicManager.getInstance()
				.getMusicManager(event.getGuild())
				.getScheduler().info(this);
	}

	@Override
	public String name() {
		return "info";
	}

	@Override
	public String help() {
		return "get the information of what's being played, what was played and what will be played";
	}
}
