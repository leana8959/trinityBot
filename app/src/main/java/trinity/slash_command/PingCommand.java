package trinity.slash_command;

import net.dv8tion.jda.api.events.interaction.command.SlashCommandInteractionEvent;
import net.dv8tion.jda.api.interactions.commands.build.Commands;
import net.dv8tion.jda.api.interactions.commands.build.SlashCommandData;

public class PingCommand extends SlashCommand {
	@Override
	public String name() {
		return "ping";
	}

	@Override
	public String help() {
		return "Show gateway ping";
	}

	@Override
	public void handle(SlashCommandInteractionEvent event) {
		event.getJDA().getRestPing().queue(
				ping -> event.replyFormat(
						"Rest request ping %sms\nGateway ping: %sms",
						ping, event.getJDA().getGatewayPing()).queue()
		);
	}

}
