package trinity.slash_command;

import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.events.interaction.command.SlashCommandInteractionEvent;
import net.dv8tion.jda.api.interactions.commands.build.Commands;
import net.dv8tion.jda.api.interactions.commands.build.SlashCommandData;
import net.dv8tion.jda.api.managers.AudioManager;
import trinity.SlashCommandManager;
import trinity.music.GuildMusicManager;

import java.awt.*;

public class LeaveCommand extends MusicSlashCommand {
	@Override
	public String name() {
		return "leave";
	}

	@Override
	public String help() {
		return "Make trinity leave the voice channel";
	}

	@Override
	public void handleDefer(SlashCommandInteractionEvent event) {

		if (!selfInVoiceChannel(event)) {
			EmbedBuilder builder = new EmbedBuilder()
					.setTitle("I have to be in a audio channel for this to work")
					.setColor(Color.RED);
			event.getHook().editOriginalEmbeds(builder.build()).queue();
			return;
		}

		SlashCommandManager.getInstance().invokeDefer("stop", event);

		final AudioManager audioManager = event.getGuild().getAudioManager();
		audioManager.closeAudioConnection();
		EmbedBuilder builder = new EmbedBuilder()
				.setTitle("Pack up your stuff, the party is over");
		event.getHook().editOriginalEmbeds(builder.build()).queue();

	}

}
