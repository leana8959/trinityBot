package trinity.slash_command;

import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.entities.GuildVoiceState;
import net.dv8tion.jda.api.entities.Member;
import net.dv8tion.jda.api.events.interaction.command.SlashCommandInteractionEvent;
import net.dv8tion.jda.api.interactions.commands.build.Commands;
import net.dv8tion.jda.api.interactions.commands.build.SlashCommandData;
import trinity.music.GuildMusicManager;
import trinity.music.MusicManager;

import java.awt.*;

public class ResumeCommand extends MusicSlashCommand{
	@Override
	public void handleDefer(SlashCommandInteractionEvent event) {

		if (!selfInVoiceChannel(event)) {
			EmbedBuilder builder = new EmbedBuilder()
					.setTitle("I have to be in a audio channel for this to work")
					.setColor(Color.RED);
			event.getHook().editOriginalEmbeds(builder.build()).queue();
			return;
		}

		final MusicManager musicManager = GuildMusicManager.getInstance().getMusicManager(event.getGuild());
		musicManager.getScheduler().resume(this);
	}

	@Override
	public String name() {
		return "resume";
	}

	@Override
	public String help() {
		return "resume the music";
	}

}
