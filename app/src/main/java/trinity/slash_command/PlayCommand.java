package trinity.slash_command;

import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.events.interaction.command.SlashCommandInteractionEvent;
import net.dv8tion.jda.api.interactions.commands.OptionType;
import net.dv8tion.jda.api.interactions.commands.build.Commands;
import net.dv8tion.jda.api.interactions.commands.build.SlashCommandData;
import trinity.SlashCommandManager;
import trinity.music.MusicManager;
import trinity.music.GuildMusicManager;

import java.awt.*;

public class PlayCommand extends MusicSlashCommand {

	private boolean search;
	private boolean interrupt;
	private boolean insert;
	private String query;


	@Override
	public String name() {
		return "play";
	}

	@Override
	public String help() {
		return "Play a song or a playlist";
	}

	@Override
	public void handleDefer(SlashCommandInteractionEvent event) {

		if (!memberInVoiceChannel(event)) {
			EmbedBuilder builder = new EmbedBuilder()
					.setTitle("You have to be in a voice channel for this to work")
					.setColor(Color.RED);
			event.getHook().editOriginalEmbeds(builder.build()).queue();
			return;
		}

		if (!selfInVoiceChannel(event)) {
			EmbedBuilder builder = new EmbedBuilder()
					.setTitle("Use the join command first")
					.setColor(Color.RED);
			event.getHook().editOriginalEmbeds(builder.build()).queue();
			return;
		}

		this.query = event.getOption("query").getAsString();
		this.search = false;
		this.interrupt = false;
		this.insert = true;
		if (event.getOption("search") != null) {
			this.search = event.getOption("search").getAsBoolean();
		}
		if (event.getOption("interrupt") != null) {
			this.interrupt = event.getOption("interrupt").getAsBoolean();
		}
		if (event.getOption("insert") != null) {
			this.insert = event.getOption("insert").getAsBoolean();
		}

		final MusicManager musicManager = GuildMusicManager.getInstance().getMusicManager(event.getGuild());
		if (search) query = "ytsearch:" + query;

		GuildMusicManager.getInstance()
				.getMusicManager(event.getGuild())
				.getScheduler()
				.play(this);

	}

	public boolean isInterrupt() {
		return interrupt;
	}

	public boolean isInsert() {
		return insert;
	}

	public boolean isSearch() {
		return search;
	}

	public String getQuery() {
		return query;
	}

	@Override
	public SlashCommandData getSlashCommand() {
		return super.getSlashCommand()
				.addOption(OptionType.STRING, "query", "link to the song or playlist or a keyword", true)
				.addOption(OptionType.BOOLEAN, "search", "search on youtube, default is false", false)
				.addOption(OptionType.BOOLEAN, "interrupt", "kill the current song, default is false", false)
				.addOption(OptionType.BOOLEAN, "insert", "play this song immediately after, default is true if interrupt is false", false);
	}
}
