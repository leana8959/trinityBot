package trinity.slash_command;

import net.dv8tion.jda.api.events.interaction.command.SlashCommandInteractionEvent;

public abstract class MusicSlashCommand extends SlashCommand {

	public SlashCommandInteractionEvent event;


	public boolean memberInVoiceChannel(SlashCommandInteractionEvent event) {
		return event.getMember().getVoiceState().inAudioChannel();
	}

	public boolean selfInVoiceChannel(SlashCommandInteractionEvent event) {
		return event.getGuild().getSelfMember().getVoiceState().inAudioChannel();
	}

	public SlashCommandInteractionEvent getEvent() {
		return event;
	}

	@Override
	public void handle(SlashCommandInteractionEvent event) {
		this.event = event;
		event.deferReply().queue();
		handleDefer(event);
	}

	public abstract void handleDefer(SlashCommandInteractionEvent event);

}
