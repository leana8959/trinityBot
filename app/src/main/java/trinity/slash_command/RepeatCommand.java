package trinity.slash_command;

import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.events.interaction.command.SlashCommandInteractionEvent;
import net.dv8tion.jda.api.interactions.commands.OptionType;
import net.dv8tion.jda.api.interactions.commands.build.SlashCommandData;
import trinity.music.GuildMusicManager;
import trinity.music.TrackScheduler;

import java.awt.*;

@Deprecated
public class RepeatCommand extends MusicSlashCommand {

	private boolean repeat;

	@Override
	public void handleDefer(SlashCommandInteractionEvent event) {
		if (!selfInVoiceChannel(event)) {
			EmbedBuilder builder = new EmbedBuilder()
					.setTitle("I have to be in a audio channel for this to work")
					.setColor(Color.RED);
			event.getHook().editOriginalEmbeds(builder.build()).queue();
			return;
		}
		repeat = event.getOption("on").getAsBoolean();
		GuildMusicManager.getInstance()
				.getMusicManager(event.getGuild())
				.getScheduler()
				.repeat(this);

	}

	@Override
	public String name() {
		return "repeat";
	}

	@Override
	public String help() {
		return "set repeat to on or off";
	}

	@Override
	public SlashCommandData getSlashCommand() {
		return super.getSlashCommand()
				.addOption(OptionType.BOOLEAN, "on", "set repeat mode on or off", true);
	}

	public boolean isRepeat() {
		return repeat;
	}
}
