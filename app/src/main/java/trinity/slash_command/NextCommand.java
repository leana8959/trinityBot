package trinity.slash_command;

import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.entities.GuildVoiceState;
import net.dv8tion.jda.api.entities.Member;
import net.dv8tion.jda.api.events.interaction.command.SlashCommandInteractionEvent;
import net.dv8tion.jda.api.interactions.commands.OptionType;
import net.dv8tion.jda.api.interactions.commands.build.Commands;
import net.dv8tion.jda.api.interactions.commands.build.SlashCommandData;
import trinity.music.MusicManager;
import trinity.music.GuildMusicManager;

import java.awt.*;

public class NextCommand extends MusicSlashCommand {

	private int count;
	@Override
	public String name() {
		return "next";
	}

	@Override
	public String help() {
		return "Fast forward to the next *n* song";
	}

	@Override
	public void handleDefer(SlashCommandInteractionEvent event) {

		if (!selfInVoiceChannel(event)) {
			EmbedBuilder builder = new EmbedBuilder()
					.setTitle("I have to be in a audio channel for this to work")
					.setColor(Color.RED);
			event.getHook().editOriginalEmbeds(builder.build()).queue();
			return;
		}

		  count = 1;
		if (event.getOption("count") != null) {
			count = event.getOption("count").getAsInt();
		}

		final MusicManager musicManager = GuildMusicManager.getInstance().getMusicManager(event.getGuild());
		musicManager.getScheduler().next(this);


	}

	@Override
	public SlashCommandData getSlashCommand() {
		return super.getSlashCommand()
				.addOption(OptionType.INTEGER, "count", "the quantity of songs to skip", false);
	}

	public int getCount() {
		return count;
	}
}
