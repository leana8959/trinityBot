package trinity.slash_command;

import net.dv8tion.jda.api.events.interaction.command.SlashCommandInteractionEvent;
import net.dv8tion.jda.api.interactions.commands.SlashCommandInteraction;
import net.dv8tion.jda.api.interactions.commands.build.Commands;
import net.dv8tion.jda.api.interactions.commands.build.SlashCommandData;

public abstract class SlashCommand {

	public abstract String name();

	public abstract String help();

	public abstract void handle(SlashCommandInteractionEvent event);

	public SlashCommandData getSlashCommand() {
		return Commands.slash(this.name(), this.help());
	}

}
