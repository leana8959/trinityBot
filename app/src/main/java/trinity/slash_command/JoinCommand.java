package trinity.slash_command;

import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.Permission;
import net.dv8tion.jda.api.entities.Member;
import net.dv8tion.jda.api.entities.channel.unions.AudioChannelUnion;
import net.dv8tion.jda.api.events.interaction.command.SlashCommandInteractionEvent;
import net.dv8tion.jda.api.interactions.commands.build.Commands;
import net.dv8tion.jda.api.interactions.commands.build.SlashCommandData;
import net.dv8tion.jda.api.managers.AudioManager;

import java.awt.*;

public class JoinCommand extends MusicSlashCommand {
	@Override
	public String name() {
		return "join";
	}

	@Override
	public String help() {
		return "Have trinity join your voice channel";
	}

	@Override
	public void handleDefer(SlashCommandInteractionEvent event) {

		if (selfInVoiceChannel(event)) {
			EmbedBuilder builder = new EmbedBuilder()
					.setTitle("I'm already in a audio channel")
					.setColor(Color.RED);
			event.getHook().editOriginalEmbeds(builder.build()).queue();
			return;
		}

		if (!memberInVoiceChannel(event)) {
			EmbedBuilder builder = new EmbedBuilder()
					.setTitle("You have to be in a voice channel for this to work")
					.setColor(Color.RED);
			event.getHook().editOriginalEmbeds(builder.build()).queue();
			return;
		}

		final Member member = event.getMember();
		final Member selfMember = event.getGuild().getSelfMember();
		final AudioChannelUnion memberAudioChannel = member.getVoiceState().getChannel();
		if (!selfMember.hasPermission(memberAudioChannel, Permission.VOICE_CONNECT)) {
			EmbedBuilder builder = new EmbedBuilder()
					.setTitle("I don't have access to the audio channel...")
					.setColor(Color.RED);
			event.getHook().editOriginalEmbeds(builder.build()).queue();
			return;
		}

		final AudioManager audioManager = event.getGuild().getAudioManager();
		audioManager.openAudioConnection(memberAudioChannel);
		EmbedBuilder builder = new EmbedBuilder()
				.setTitle("Connected !")
				.setColor(Color.GREEN);
		event.getHook().editOriginalEmbeds(builder.build()).queue();
	}

}
