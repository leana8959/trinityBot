package trinity.slash_command;

import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.events.interaction.command.SlashCommandInteractionEvent;
import net.dv8tion.jda.api.interactions.commands.build.Commands;
import net.dv8tion.jda.api.interactions.commands.build.SlashCommandData;
import trinity.music.MusicManager;
import trinity.music.GuildMusicManager;

import java.awt.*;

public class PauseCommand extends MusicSlashCommand {
	@Override
	public String name() {
		return "pause";
	}

	@Override
	public String help() {
		return "Pause the music";
	}

	@Override
	public void handleDefer(SlashCommandInteractionEvent event) {

		if (!selfInVoiceChannel(event)) {
			EmbedBuilder builder = new EmbedBuilder()
					.setTitle("I have to be in a audio channel for this to work")
					.setColor(Color.RED);
			event.getHook().editOriginalEmbeds(builder.build()).queue();
			return;
		}

		final MusicManager musicManager = GuildMusicManager.getInstance().getMusicManager(event.getGuild());
		musicManager.getScheduler().pause(this);
	}

}
